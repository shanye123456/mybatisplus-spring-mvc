package mybatisplus.spring.mvc.admin.common.enums;

public interface ErrorEnumInterface {
    String getErrorMessage();

    int getErrorCode();

    int getParentCode();
}
