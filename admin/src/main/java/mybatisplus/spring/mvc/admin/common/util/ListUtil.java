package mybatisplus.spring.mvc.admin.common.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ListUtil {

    public static boolean isNull(List<?> list) {
        return list == null || list.size() <= 0 || list.get(0) == null;
    }

    public static boolean notNull(List<?> list) {
        return list != null && list.size() > 0 && list.get(0) != null;
    }

    public static <T> T[] toArray(List<T> list, Class<? extends T> newType) {
        if (ListUtil.isNull(list)) return null;
        int newLength = list.size();
        T[] ret = (newType == Object[].class) ? (T[]) new Object[newLength] : (T[]) Array.newInstance(newType, newLength);
        for (int i = 0; i < ret.length; i++) if (list.get(i) != null) ret[i] = list.get(i);
        return ret;
    }

    public static <T> List<T> toList(String listData, String split, Class<? extends T> newType) {
        if (StringUtil.isNull(listData) || StringUtil.isNull(split)) return null;
        Object[] array = listData.split(split);
        List<T> ret = new ArrayList<>(array.length);
        for (Object obj : array) ret.add((T) obj);
        return ret;
    }

    public static int size(List<?> list) {
        if (list == null) return 0;
        return list.size();
    }
}
