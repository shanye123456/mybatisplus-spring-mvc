package mybatisplus.spring.mvc.admin.common.dynaminc.db;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource{
 
    @Override
    public Object determineCurrentLookupKey() {
        return CustomerContextHolder.getCustomerType();
    }
}