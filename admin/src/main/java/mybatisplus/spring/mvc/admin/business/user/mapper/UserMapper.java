package mybatisplus.spring.mvc.admin.business.user.mapper;

import mybatisplus.spring.mvc.admin.business.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
public interface UserMapper extends BaseMapper<User> {

}
