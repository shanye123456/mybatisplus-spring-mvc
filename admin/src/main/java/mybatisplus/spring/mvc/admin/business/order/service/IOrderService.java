package mybatisplus.spring.mvc.admin.business.order.service;

import mybatisplus.spring.mvc.admin.business.order.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
public interface IOrderService extends IService<Order> {

}
