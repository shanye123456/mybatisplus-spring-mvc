package mybatisplus.spring.mvc.admin.common.base;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 测试自定义实体父类 ， 这里可以放一些公共字段信息
 * </p>
 */
@Data
public class SuperEntity implements Serializable {
    // 这里可以写自己的公共属性、注意不要让 mp 扫描到误以为是实体 Base 的操作
}
