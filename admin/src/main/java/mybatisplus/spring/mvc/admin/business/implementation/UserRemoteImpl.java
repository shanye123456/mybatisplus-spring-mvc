package mybatisplus.spring.mvc.admin.business.implementation;

import mybatisplus.spring.mvc.admin.business.user.entity.User;
import mybatisplus.spring.mvc.admin.business.user.service.IUserService;
import mybatisplus.spring.mvc.api.remote.UserRemote;
import mybatisplus.spring.mvc.api.req.UserQueryReq;
import mybatisplus.spring.mvc.api.res.UserQueryRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRemoteImpl implements UserRemote {

    @Autowired
    private IUserService userService;
    @Override
    public UserQueryRes queryUserByReq(UserQueryReq queryUserReq) {

        User user = userService.getById(queryUserReq.getId());
        return UserQueryRes.builder().age(user.getAge()).name(user.getName()).id(user.getId()).type(user.getType()).build();
    }

}
