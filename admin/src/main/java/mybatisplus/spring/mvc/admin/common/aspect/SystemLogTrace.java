package mybatisplus.spring.mvc.admin.common.aspect;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class SystemLogTrace {

    @Pointcut("execution(* mybatisplus.spring.mvc.admin.business.*.controller.*.*(..))")
    public void logPointCut() {}


    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {

        long beginTime = System.currentTimeMillis();

        Object result = point.proceed();

        Object[] args = point.getArgs();

        Object signature = point.getSignature();

        long time = System.currentTimeMillis() - beginTime;

        log.info("params method={},args={}",signature, JSON.toJSONString(args));

        log.info("result method={},result={}",signature, JSON.toJSONString(result));

        log.info("execute method {} spend time={}ms",signature,time);

        return result;
    }

}
