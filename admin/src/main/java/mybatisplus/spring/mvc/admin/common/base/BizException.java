package mybatisplus.spring.mvc.admin.common.base;


import mybatisplus.spring.mvc.admin.common.enums.ErrorEnumInterface;

public class BizException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private int code;
    private boolean propertiesKey;

    public BizException(String message) {
        super(message);
        this.propertiesKey = true;
    }

    public BizException(ErrorEnumInterface errorEnumInterface) {
        this(errorEnumInterface.getErrorCode(), errorEnumInterface.getErrorMessage());
    }

    public BizException(int code, String message, Throwable cause) {
        this(code, message, cause, true);
    }

    public BizException(int code, String message) {
        super(message);
        this.propertiesKey = true;
        this.setCode(code);
    }

    public BizException(int code, String message, boolean propertiesKey) {
        super(message);
        this.propertiesKey = true;
        this.setCode(code);
        this.setPropertiesKey(propertiesKey);
    }

    public BizException(int code, String message, Throwable cause, boolean propertiesKey) {
        super(message, cause);
        this.propertiesKey = true;
        this.setCode(code);
        this.setPropertiesKey(propertiesKey);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
        this.propertiesKey = true;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isPropertiesKey() {
        return propertiesKey;
    }

    public void setPropertiesKey(boolean propertiesKey) {
        this.propertiesKey = propertiesKey;
    }
}
