package mybatisplus.spring.mvc.admin.business.order.mapper;

import mybatisplus.spring.mvc.admin.business.order.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
