package mybatisplus.spring.mvc.admin.business.user.service;

import mybatisplus.spring.mvc.admin.business.user.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
public interface IUserService extends IService<User> {

    User findUserById1(String id);

    User findUserById2(String id);
}
