package mybatisplus.spring.mvc.admin.common.util;

/**
 * 微服务提供者信息
 *
 * @author code-generator
 * @date 2019-05-23 16:09:27
 */
public class ServiceConst {

    public static final String ORDER_CENTER = "order-center";
    public static final String ENTERPRISE_USER = "enterprise-user";
    public static final String COMPONENT_FACADE = "component";
    public static final String USER_CENTER = "user-center";
    public static final String PAY_CENTER = "pay-center";
}
