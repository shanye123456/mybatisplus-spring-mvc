package mybatisplus.spring.mvc.admin.common.enums;

public enum WordEnum {

    LOW(1, "小写"),
    NORMAL(2, "不变"),
    UP(3, "大写");

    private int code;
    private String name;

    WordEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
