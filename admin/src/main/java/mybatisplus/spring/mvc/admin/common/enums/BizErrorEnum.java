package mybatisplus.spring.mvc.admin.common.enums;
/**
 * @author code-generator
 */

public enum BizErrorEnum implements ErrorEnumInterface {
    /**
     * 错误码分内外两种
     * 对内使用最细粒度错误吗，对外使用统一错误码
     * 对外统一使用本类型第一个错误码。
     */
    //TODO 各业务模块自行替换下面的定义
    ERROR_ID_CAN_NOT_BE_NULL(30000,30001,"数据存在依赖不能删除");


    private final int errorCode;
    private final int parentCode;
    private final String errorMessage;

    BizErrorEnum(int parentCode, int errorCode, String errorMessage) {
        this.parentCode = parentCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public int getParentCode() {
        return parentCode;
    }

    public BizErrorEnum getOutError() {
        return getErrorByCode(getParentCode());
    }

    public static BizErrorEnum getErrorByCode(int code) {
        for (BizErrorEnum errorEnum : values()) {
            if (errorEnum.getErrorCode() == code) {
                return errorEnum;
            }
        }
        return null;
    }
}
