package mybatisplus.spring.mvc.admin.common.enums;

public enum ErrorEnum implements ErrorEnumInterface {
    /**
     * 错误码分内外两种
     * 对内使用最细粒度错误吗，对外使用统一错误码
     * 对外统一使用本类型第一个错误码。
     */
    SYSTEM_ERROR(10000, 10000, "系统开小差，请稍后再试"),
    SYSTEM_RISK_CHECK_ERROR(10000, 10010, "用户存在风险,请稍后再试"),
    ERROR_CLIENT_ABORT(10000, 10020, "客户端断开连接"),
    REQUEST_INVALID(11000, 11000, "签名异常"),
    ERROR_BIZ_FAIL(20000, 20001, "业务失败"),
    ERROR_PARAM(12000, 12002, "参数校验错误"),
    ERROR_PARAM_FORMAT(12000, 12003, "参数类型错误"),
    ERROR_CGUID_VALID(13000, 13001, "Cguid无效"),
    ERROR_CGUID_CHECK(13000, 13002, "Cguid请求异常"),
    ERROR_IP_CHECK(13000, 13003, "请求过于频繁"),
    ERROR_IP_SOURCE(13000, 13004, "请求来源异常"),
    ERROR_TOKEN_VALID(14000, 14001, "Token过期"),
    ERROR_TOKEN_CHECK(14000, 14002, "Token验证异常"),
    ERROR_ADMIN_RIGHT(14000, 14003, "权限不足"),
    ERROR_ADMIN_RIGHT_NULL(14000, 14004, "权限不为空"),
    ERROR_RE_LOGIN(14000, 14005, "请重新登陆"),
    ERROR_REDIS_LOCK_TIME_OUT(15000, 15001, "系统开小差，请稍后再试"),
    ERROR_WRITE(16000, 16001, "渲染界面错误"),
    ERROR_BIZ_CKAFKA_TOPIC_EMPTY(21000, 21002, "CKafka消息主题不能为空"),
    ERROR_BIZ_CKAFKA_CREATE_TOPIC_FAIL(21000, 21003, "CKafka创建主题失败"),
    ERROR_BIZ_CKAFKA_SEND_MSG_FAIL(21000, 21004, "CKafka发送消息失败"),
    ERROR_BIZ_CKAFKA_KEY_EMPTY(21000, 21005, "CKafka消息KEY不能为空"),
    ERROR_BIZ_CKAFKA_VALUE_EMPTY(21000, 21006, "CKafka消息VALUE不能为空");

    private final int errorCode;
    private final int parentCode;
    private final String errorMessage;

    private ErrorEnum(int parentCode, int errorCode, String errorMessage) {
        this.parentCode = parentCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public int getParentCode() {
        return this.parentCode;
    }

    public ErrorEnum getOutError() {
        return getErrorByCode(this.getParentCode());
    }

    public static ErrorEnum getErrorByCode(int code) {
        ErrorEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            ErrorEnum errorEnum = var1[var3];
            if (errorEnum.getErrorCode() == code) {
                return errorEnum;
            }
        }

        return null;
    }
}
