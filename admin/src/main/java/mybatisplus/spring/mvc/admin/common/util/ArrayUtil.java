package mybatisplus.spring.mvc.admin.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArrayUtil {

    public static boolean notNull(Object[] array) {
        if (array != null && array.length > 0) return true;
        return false;
    }

    public static boolean isNull(Object[] array) {
        return !notNull(array);
    }

    public static String toString(Object[] array) {
        String ret = "";
        if (array != null && array.length > 0) {
            for (Object s : array) {
                if (s == null) continue;
                ret += s + ",";
            }
            return ret.substring(0, ret.length() - 1);
        }
        return ret;
    }

    public static String toString(Collection c, String split) {
        StringBuilder stringBuilder = new StringBuilder();
        if (c != null && c.size() > 0) {
            for (Object s : c) {
                if (s != null) stringBuilder.append(String.valueOf(s)).append(split);
            }
            String ret = stringBuilder.toString();
            return ret.substring(0, ret.length() - split.length());
        }
        return stringBuilder.toString();
    }

    public static List toList(Object[] array) {
        List list = new ArrayList();
        if (array != null && array.length > 0) {
            for (Object obj : array) list.add(obj);
        }
        return list;
    }

    public static boolean contains(String[] array, String str) {
        if (array != null && array.length > 0 && str != null) {
            for (String s : array) {
                if (str.equals(s)) return true;
            }
        }
        return false;
    }

    public static boolean dimContains(String[] array, String str) {
        if (array != null && array.length > 0 && str != null) {
            for (String s : array) {
                if (s.contains(str)) return true;
            }
        }
        return false;
    }

}
