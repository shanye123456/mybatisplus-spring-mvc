package mybatisplus.spring.mvc.admin.business.integration.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class UserResultModel {

    /**
     * 用户名
     */
    private String name;

    /**
     * 用户年龄
     */
    private Integer age;

    /**
     * 0、禁用 1、正常
     */
    private Integer type;

}
