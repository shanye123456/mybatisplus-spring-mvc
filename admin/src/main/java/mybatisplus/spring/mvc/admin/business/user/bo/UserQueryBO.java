package mybatisplus.spring.mvc.admin.business.user.bo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
public class UserQueryBO {

    @NotBlank(
            message = "用户id不能为空"
    )
    private String id;

    @JSONField(format  = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
