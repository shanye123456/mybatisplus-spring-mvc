package mybatisplus.spring.mvc.admin.common.dynaminc.db;

public class CustomerContextHolder {

    private static ThreadLocal<String> contextHolder = new ThreadLocal<String>();
     
    public static void setCustomerType(String customerType) {
        contextHolder.set(customerType);
    }


    public static String getCustomerType() {

        return contextHolder.get();
    }
}