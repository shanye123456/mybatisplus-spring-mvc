package mybatisplus.spring.mvc.admin.common.util;

/**
 * 缓存常量类
 *
 * @author code-generator
 * @date 2019-05-23 16:09:27
 */
public class CacheKey {

    public static final String APP_NAME = "shop:";

    public static final String KEY_PRODUCT = APP_NAME + "product:";

    public static final String KEY_PHONE = KEY_PRODUCT + "phone:";

    public static final String KEY_ORDER_SERVICE_ERR_LOCK = APP_NAME + "shop:order:err:task";

    public static final String KEY_RECHARGE_SERVICE_ERR_LOCK = APP_NAME + "shop:recharge:err:task";

}
