package mybatisplus.spring.mvc.admin.business.order.mapper;

import mybatisplus.spring.mvc.admin.business.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
public interface OrderMapper extends BaseMapper<Order> {

}
