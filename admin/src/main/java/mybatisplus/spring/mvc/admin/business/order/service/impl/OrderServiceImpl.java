package mybatisplus.spring.mvc.admin.business.order.service.impl;

import mybatisplus.spring.mvc.admin.business.order.entity.Order;
import mybatisplus.spring.mvc.admin.business.order.mapper.OrderMapper;
import mybatisplus.spring.mvc.admin.business.order.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

}
