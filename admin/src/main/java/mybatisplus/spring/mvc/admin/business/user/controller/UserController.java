package mybatisplus.spring.mvc.admin.business.user.controller;


import mybatisplus.spring.mvc.admin.business.user.bo.UserQueryBO;
import mybatisplus.spring.mvc.admin.business.user.entity.User;
import mybatisplus.spring.mvc.admin.business.user.service.IUserService;
import mybatisplus.spring.mvc.admin.business.user.vo.UserQueryVO;
import mybatisplus.spring.mvc.admin.common.base.JsonResVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 系统用户表 前端控制器
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
@RestController
@RequestMapping("/user/")
public class UserController {
    @Autowired
    private IUserService iUserService;

    @PostMapping("/queryUser")
    public JsonResVo queryUser(@RequestBody @Validated UserQueryBO bo){
        User user = iUserService.getById(bo.getId());
        UserQueryVO userQueryVO = UserQueryVO.builder()
                .age(user.getAge())
                .name(user.getName())
                .createTime(user.getCreateTime()).build();
        return JsonResVo.buildSuccess(userQueryVO);
    }
    @GetMapping("/test")
    public String test(String age){
        System.out.print(age);
        return "hello test";
    }
}
