package mybatisplus.spring.mvc.admin.business.integration;

import mybatisplus.spring.mvc.admin.business.integration.model.UserQueryModel;
import mybatisplus.spring.mvc.admin.business.integration.model.UserResultModel;
import mybatisplus.spring.mvc.api.remote.UserRemote;
import mybatisplus.spring.mvc.api.req.UserQueryReq;
import mybatisplus.spring.mvc.api.res.UserQueryRes;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserRemoteInte {

    @Resource
    private UserRemote userRemote;

    public UserResultModel queryUser(UserQueryModel userQueryModel) {
        UserQueryReq queryUserReq = new UserQueryReq();
        queryUserReq.setId(userQueryModel.getId());
        UserQueryRes userQueryRes = userRemote.queryUserByReq(queryUserReq);
        return UserResultModel.builder().age(userQueryRes.getAge())
                .name(userQueryRes.getName())
                .type(userQueryRes.getType()).build();
    }
}
