package mybatisplus.spring.mvc.admin.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class StringUtil {

    public static final String splitFlag = ",";

    public static int length(String s) {
        if (s == null) return 0;
        else return s.length();
    }

    public static String substring(String s, int length) {
        if (length <= 0) return "";
        if (length(s) <= length) return s;
        else return s.substring(0, length);
    }

    public static boolean isNull(String s) {
        return s == null || s.length() <= 0;
    }

    public static boolean notNull(String s) {
        return !isNull(s);
    }

    public static boolean containsArrayString(String sourceString, String delString, String split) {
        if (isNull(sourceString) || isNull(delString) || isNull(split)) return false;
        String[] sArray = sourceString.split(split);
        for (String s : sArray) {
            if (s.equals(delString)) return true;
        }
        return false;
    }

    public static boolean isChinaPhoneLegal(String phone) throws PatternSyntaxException {
        if (isNull(phone)) return false;
        String regExp = "^1\\d{10}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(phone);
        return m.matches();
    }
}
