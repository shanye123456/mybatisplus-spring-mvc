package mybatisplus.spring.mvc.admin.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectUtil {

    public static String getFieldGetterName(String str) {
        return "get" + str.substring(0, 1).toUpperCase() + str.substring(1, str.length());
    }

    public static String getFieldSetterName(String str) {
        return "set" + str.substring(0, 1).toUpperCase() + str.substring(1, str.length());
    }

    public static Method getFieldGetter(Class<?> clazz, String fieldName) {
        try {
            return clazz.getMethod(getFieldGetterName(fieldName));
        } catch (SecurityException ex) {
            return null;
        } catch (NoSuchMethodException ex) {
            return null;
        }
    }

    public static Method getFieldSetter(Class<?> clazz, String fieldName) {
        try {
            return clazz.getMethod(getFieldSetterName(fieldName), getFieldType(clazz, fieldName));
        } catch (SecurityException ex) {
            return null;
        } catch (NoSuchMethodException ex) {
            return null;
        }
    }

    public static Class<?> getFieldType(Class<?> clazz, String fieldName) {
        try {
            return clazz.getDeclaredField(fieldName).getType();
        } catch (SecurityException e) {
            return null;
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    public static Object trans(Class<?> source, Object sourceObject, Class<?> des, Object desObject) {
        try {
            Class<?> s = source;
            Object oldData = sourceObject;
            Field sf[] = s.getDeclaredFields();// 把属性的信息提取出来，并且存放到field类的对象中，因为每个field的对象只能存放一个属性的信息所以要用数组去接收

            Class<?> d = des;
            for (int i = 0; i < sf.length; i++) {
                Method M;
                try {
                    M = s.getDeclaredMethod("get" + FirstLetter(sf[i].getName()));
                } catch (Exception e) {
                    continue;
                }
                Object valueObject = M.invoke(oldData);

                if (valueObject == null) continue;
                Method m = d.getDeclaredMethod("set" + FirstLetter(sf[i].getName()), sf[i].getType());
                if (valueObject instanceof Integer) {
                    m.invoke(desObject, (int) valueObject);
                } else if (valueObject instanceof Long) {
                    m.invoke(desObject, (long) valueObject);
                } else if (valueObject instanceof Double) {
                    m.invoke(desObject, (double) valueObject);
                } else {
                    m.invoke(desObject, sf[i].getType().cast(valueObject));
                }
            }
            return desObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过get方法获取值
     *
     * @param sourceObject  对象
     * @param AttributeName 对象的属性名
     * @return 该属性的值
     */
    public static Object getValue(Object sourceObject, String AttributeName) {
        try {
            Class<?> s = sourceObject.getClass();
            Method M = null;
            M = s.getDeclaredMethod("get" + FirstLetter(AttributeName));
            if (M != null) return M.invoke(sourceObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过set方法获取值
     *
     * @param sourceObject  对象
     * @param AttributeName 对象的属性名
     * @return 该属性的值
     */
    public static Object setValue(Object sourceObject, String AttributeName, Object value) {
        try {
            invokeMethod(sourceObject, "set" + FirstLetter(AttributeName),
                    new Class[]{value.getClass()}, new Object[]{value});
//            Class<?> s = sourceObject.getClass();
//            Method M = s.getDeclaredMethod("set" + FirstLetter(AttributeName), value.getClass());
//            if (M != null) return M.invoke(sourceObject, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String FirstLetter(String fldName) {
        String first = fldName.substring(0, 1).toUpperCase();
        String rest = fldName.substring(1, fldName.length());
        return new StringBuffer(first).append(rest).toString();
    }

    /**
     * 循环向上转型, 获取对象的 DeclaredMethod
     *
     * @param object         : 子类对象
     * @param methodName     : 父类中的方法名
     * @param parameterTypes : 父类中的方法参数类型
     * @return 父类中的方法对象
     */

    public static Method getDeclaredMethod(Object object, String methodName, Class<?>... parameterTypes) {
        Method method = null;

        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                method = clazz.getDeclaredMethod(methodName, parameterTypes);
                return method;
            } catch (Exception e) {
                // 这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                // 如果这里的异常打印或者往外抛，则就不会执行clazz=clazz.getSuperclass(),最后就不会进入到父类中了
            }
        }

        return null;
    }

    /**
     * 直接调用对象方法, 而忽略修饰符(private, protected, default)
     *
     * @param object         : 子类对象
     * @param methodName     : 父类中的方法名
     * @param parameterTypes : 父类中的方法参数类型
     * @param parameters     : 父类中的方法参数
     * @return 父类中方法的执行结果
     */

    public static Object invokeMethod(Object object, String methodName, Class<?>[] parameterTypes,
                                      Object[] parameters) {
        // 根据 对象、方法名和对应的方法参数 通过反射 调用上面的方法获取 Method 对象
        Method method = getDeclaredMethod(object, methodName, parameterTypes);

        // 抑制Java对方法进行检查,主要是针对私有方法而言
        method.setAccessible(true);

        try {
            if (null != method) {

                // 调用object 的 method 所代表的方法，其方法的参数是 parameters
                return method.invoke(object, parameters);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 循环向上转型, 获取对象的 DeclaredField
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return 父类中的属性对象
     */

    public static Field getDeclaredField(Object object, String fieldName) {
        Field field = null;

        Class<?> clazz = object.getClass();

        for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                field = clazz.getDeclaredField(fieldName);
                return field;
            } catch (Exception e) {
                // 这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                // 如果这里的异常打印或者往外抛，则就不会执行clazz = clazz.getSuperclass(),最后就不会进入到父类中了

            }
        }

        return null;
    }

    /**
     * 直接设置对象属性值, 忽略 private/protected 修饰符, 也不经过 setter
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @param value     : 将要设置的值
     */

    public static void setFieldValue(Object object, String fieldName, Object value) {

        // 根据 对象和属性名通过反射 调用上面的方法获取 Field对象
        Field field = getDeclaredField(object, fieldName);

        // 抑制Java对其的检查
        field.setAccessible(true);

        try {
            // 将 object 中 field 所代表的值 设置为 value
            field.set(object, value);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    /**
     * 直接读取对象的属性值, 忽略 private/protected 修饰符, 也不经过 getter
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return : 父类中的属性值
     */

    public static Object getFieldValue(Object object, String fieldName) {

        // 根据 对象和属性名通过反射 调用上面的方法获取 Field对象
        Field field = getDeclaredField(object, fieldName);

        // 抑制Java对其的检查
        field.setAccessible(true);

        try {
            // 获取 object 中 field 所代表的属性值
            return field.get(object);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
