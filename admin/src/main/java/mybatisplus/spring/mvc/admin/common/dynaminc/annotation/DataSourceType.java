package mybatisplus.spring.mvc.admin.common.dynaminc.annotation;


public enum DataSourceType {
    WRITE("dataSourceWrite", "写库"),
    READ("dataSourceRead", "读库");

    DataSourceType(String source, String desc) {
        this.source = source;
        this.desc = desc;
    }

    String source;

    String desc;

    public String getSource() {
        return source;
    }

    public String getDesc() {
        return desc;
    }
}
