package mybatisplus.spring.mvc.admin.business.order.service.impl;

import mybatisplus.spring.mvc.admin.business.order.entity.OrderDetail;
import mybatisplus.spring.mvc.admin.business.order.mapper.OrderDetailMapper;
import mybatisplus.spring.mvc.admin.business.order.service.IOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单详情表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

}
