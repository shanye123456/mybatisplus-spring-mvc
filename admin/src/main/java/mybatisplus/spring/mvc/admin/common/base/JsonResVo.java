package mybatisplus.spring.mvc.admin.common.base;

import mybatisplus.spring.mvc.admin.common.enums.ErrorEnum;
import mybatisplus.spring.mvc.admin.common.enums.ErrorEnumInterface;

public class JsonResVo {
    /**
     * 错误码
     */
    private int code;
    /**
     * 错误提示信息
     */
    private String errmsg;

    /**
     * 业务数据
     */
    private Object data;

    public JsonResVo() {
    }

    public static JsonResVo buildSuccess() {
        return buildSuccess((Object)null);
    }

    public static JsonResVo buildSuccess(Object data) {
        return build(ResultStatusCode.OK.getErrcode(), (String)null, data);
    }

    public static JsonResVo buildFail(ErrorEnum errorEnum) {
        return buildErrorResult(errorEnum.getErrorCode(), errorEnum.getErrorMessage());
    }

    public static JsonResVo buildErrorResult(int code, String errmsg) {
        return buildErrorResult(code, errmsg, (Object)null);
    }

    public static JsonResVo buildErrorResult(ErrorEnumInterface errorEnum) {
        return buildErrorResult(errorEnum.getErrorCode(), errorEnum.getErrorMessage(), (Object)null);
    }

    public static JsonResVo buildErrorResult(ErrorEnumInterface errorEnum,Object data) {
        return buildErrorResult(errorEnum.getErrorCode(), errorEnum.getErrorMessage(), data);
    }

    public static JsonResVo buildErrorResult(int code, String errmsg, Object data) {
        return build(code, errmsg, data);
    }

    private static JsonResVo build(int code, String errmsg, Object data) {
        JsonResVo jsonResVo = new JsonResVo();
        jsonResVo.setCode(code);
        jsonResVo.setErrmsg(errmsg);
        jsonResVo.setData(data);
        return jsonResVo;
    }

    public long getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrmsg() {
        return this.errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
