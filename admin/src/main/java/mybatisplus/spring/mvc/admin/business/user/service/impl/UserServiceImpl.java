package mybatisplus.spring.mvc.admin.business.user.service.impl;

import mybatisplus.spring.mvc.admin.business.user.entity.User;
import mybatisplus.spring.mvc.admin.business.user.mapper.UserMapper;
import mybatisplus.spring.mvc.admin.business.user.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Cacheable(value = "userCache1", key = "#id")
    @Override
    public User findUserById1(String id) {
        return this.getById(id);
    }

    @Cacheable(value = "userCache2", key = "#id")
    @Override
    public User findUserById2(String id) {
        return this.getById(id);
    }
}
