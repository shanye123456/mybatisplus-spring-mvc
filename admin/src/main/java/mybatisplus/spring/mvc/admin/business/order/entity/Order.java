package mybatisplus.spring.mvc.admin.business.order.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 订单总金额
     */
    private Long totalAmount;

    /**
     * 支付金额
     */
    private Long payAmount;

    /**
     * 优惠金额
     */
    private Long discountAmount;

    /**
     * 基础订单状态，1待支付，2处理中，3已完成，4已取消，5已退单
     */
    private Integer orderStatus;

    /**
     * 0：未支付 1：已支付
     */
    private Integer payStatus;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 逻辑删除，1：删除，0：不删除
     */
    private Integer status;


}
