package mybatisplus.spring.mvc.admin.business.integration.model;

import lombok.Data;

@Data
public class UserQueryModel {

    private String id;
}
