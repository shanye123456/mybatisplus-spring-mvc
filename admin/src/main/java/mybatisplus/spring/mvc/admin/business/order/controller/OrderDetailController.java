package mybatisplus.spring.mvc.admin.business.order.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单详情表 前端控制器
 * </p>
 *
 * @author AutoGenerator
 * @since 2019-06-20
 */
@RestController
@RequestMapping("/order/order-detail")
public class OrderDetailController {

}
