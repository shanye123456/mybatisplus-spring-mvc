package mybatisplus.spring.mvc.admin.common.dynaminc.aop;
import mybatisplus.spring.mvc.admin.common.dynaminc.db.CustomerContextHolder;
import mybatisplus.spring.mvc.admin.common.dynaminc.annotation.DataSource;
import mybatisplus.spring.mvc.admin.common.dynaminc.annotation.DataSourceType;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(Ordered.LOWEST_PRECEDENCE-1)
public class DataSourceAspect {

    @Pointcut("@annotation(mybatisplus.spring.mvc.admin.common.dynaminc.annotation.DataSource)")
    public void dataSourcePointcut() {
    }


    @Around("dataSourcePointcut() && @annotation(anno)")
    public Object dataSourceSwitcher(ProceedingJoinPoint joinPoint, DataSource anno) throws Throwable {
        String currentDs = CustomerContextHolder.getCustomerType();
        DataSourceType value = anno.value();
        String ds=value.getSource();
        //设置数据源
        CustomerContextHolder.setCustomerType(ds);
        try {
            //执行方法
            Object result = joinPoint.proceed();
            return result;
        }catch (Exception e){
            throw e;
        }finally {
            //切换回原来的数据源（重要）
            CustomerContextHolder.setCustomerType(currentDs);
        }
    }
}