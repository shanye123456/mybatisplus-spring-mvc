-- ----------------------------
--  Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL COMMENT '用户ID',
  `name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `age` int(3) DEFAULT NULL COMMENT '用户年龄',
  `type` tinyint(2) DEFAULT '0' COMMENT '0、禁用 1、正常',
  `create_time` datetime DEFAULT NULL COMMENT '自定义填充的创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
--  Records of `t_user`
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES ('784972358981328902', 'Tom', '24', '1', '2017-06-25 20:53:33','2017-06-25 20:53:33');
INSERT INTO `t_user` VALUES ('784972358981328903', 'Jammy', '21',  '1', '2017-06-25 20:53:37','2017-06-25 20:53:37');
COMMIT;


-- ----------------------------
--  Table structure for `t_order`
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` varchar(25) NOT NULL COMMENT '订单ID',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `total_amount` bigint(10) DEFAULT NULL COMMENT '订单总金额',
  `pay_amount` bigint(10) DEFAULT NULL COMMENT '支付金额',
  `discount_amount` bigint(10) DEFAULT NULL COMMENT '优惠金额',
  `order_status` tinyint(2) DEFAULT NULL COMMENT '基础订单状态，1待支付，2处理中，3已完成，4已取消，5已退单',
  `pay_status` tinyint(2) DEFAULT NULL COMMENT '0：未支付 1：已支付',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(2) DEFAULT NULL COMMENT '逻辑删除，1：删除，0：不删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
--  Table structure for `t_order_detail`
-- ----------------------------
DROP TABLE IF EXISTS `t_order_detail`;
CREATE TABLE `t_order_detail` (
  `id` varchar(25) NOT NULL COMMENT '订单详情表ID',
  `order_id` varchar(25) DEFAULT NULL COMMENT '订单表ID',
  `product_id` varchar(25) DEFAULT NULL COMMENT '商品Id',
  `product_name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `product_count` int(8) DEFAULT NULL COMMENT '商品数量',
  `product_type` tinyint(2) DEFAULT NULL COMMENT '商品类型0:实物 1:充值',
  `product_cost_price` bigint(10) DEFAULT NULL COMMENT '商品成本价',
  `product_sale_price` bigint(10) DEFAULT NULL COMMENT '商品销售价',
  `promotion_price` bigint(10) DEFAULT NULL COMMENT '商品促销价',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单详情表';