package mybatisplus.spring.mvc.admin.code;


        import mybatisplus.spring.mvc.admin.common.util.CodeGenerator;

/**
 * 演示例子，执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
 */
public class MybatisplusCodeGenerator extends CodeGenerator {

    private static Object object = new Object();

    public static void main(String[] args) {
        codeAutoGenerator(object,"mybatisplus.spring.mvc.admin.business");
    }

}