package mybatisplus.spring.mvc.admin.business;

import mybatisplus.spring.mvc.admin.business.integration.UserRemoteInte;
import mybatisplus.spring.mvc.admin.business.integration.model.UserQueryModel;
import mybatisplus.spring.mvc.admin.business.integration.model.UserResultModel;
import mybatisplus.spring.mvc.admin.business.order.entity.Order;
import mybatisplus.spring.mvc.admin.business.order.service.IOrderService;
import mybatisplus.spring.mvc.admin.business.user.entity.User;
import mybatisplus.spring.mvc.admin.business.user.service.IUserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;


/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2018/5/3
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
public class ServiceTest {

    @Autowired
    IUserService iUserService;

    @Autowired
    IOrderService iOrderService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    UserRemoteInte userRemoteInte;

    @Test
    public void selectTest() {
        List<User> list = iUserService.list(null);
        System.out.println("****************************************");
        for (User u : list) {
            System.out.println(u.toString());
            Assert.assertNotNull("TypeEnum should not null", u.getType());
        }
        System.out.println("****************************************");
    }

    @Test
    public void saveOrderTest() {
        Order order = new Order();
        order.setPayAmount(15433L);
        System.out.println("****************************************");
        iOrderService.save(order);
        System.out.println("****************************************");
    }

    @Test
    public void updateOrderTest() {
        Order order = iOrderService.getById("1140827322676563970");
        System.out.println("****************************************");
        iOrderService.updateById(order);
        System.out.println("****************************************");
    }

    @Test
    public void redisTemplateTest() {
        redisTemplate.opsForValue().set("lixing", "jiangxi xiushui");
        System.out.println("****************************************");
        System.out.println(redisTemplate.opsForValue().get("lixing"));
        System.out.println("****************************************");
    }

    @Test
    public void dubboTest() {
        UserQueryModel userQueryModel = new UserQueryModel();
        userQueryModel.setId("1139432137614852097");
        UserResultModel userResultModel = userRemoteInte.queryUser(userQueryModel);
        System.out.println("****************************************");
        System.out.println(userResultModel);
        System.out.println("****************************************");
    }

    @Test
    public void ehCacheTest() {
        User user11 = iUserService.findUserById1("784972358981328902");

        User user2 = iUserService.findUserById2("784972358981328902");

        User user12 = iUserService.findUserById1("784972358981328902");

        PersonFactory<Person> factory = Person::new;
        Person person = factory.create("lixing","ding");
    }

    @FunctionalInterface
    public interface funInterFace {
        void method1();

        default void method2() {

        }
    }


    @FunctionalInterface
    interface PersonFactory<P extends Person> {
        P create(String firstName, String lastName);

    }

    class Person{
        public Person(String firstName, String lastName){

        }
    }
}
