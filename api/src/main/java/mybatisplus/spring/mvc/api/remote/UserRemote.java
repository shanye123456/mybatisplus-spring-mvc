package mybatisplus.spring.mvc.api.remote;


import mybatisplus.spring.mvc.api.req.UserQueryReq;
import mybatisplus.spring.mvc.api.res.UserQueryRes;

public interface UserRemote {

    UserQueryRes queryUserByReq(UserQueryReq queryUserReq);
}
