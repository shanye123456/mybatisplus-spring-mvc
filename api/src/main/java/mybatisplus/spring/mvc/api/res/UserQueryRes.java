package mybatisplus.spring.mvc.api.res;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class UserQueryRes  implements Serializable {

    private String id;

    /**
     * 用户名
     */
    private String name;

    /**
     * 用户年龄
     */
    private Integer age;

    /**
     * 0、禁用 1、正常
     */
    private Integer type;

}
