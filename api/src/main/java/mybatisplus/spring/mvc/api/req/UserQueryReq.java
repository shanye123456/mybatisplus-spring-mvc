package mybatisplus.spring.mvc.api.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserQueryReq implements Serializable {

    private String id;

}
